# Guitars with JavaScript

This is a assignment to show how easy javascript is.

## Resources

- Guitars API by Dewald Els

## Topics covered

- Functions
- DOM Manipulation
- Browser APIs
- Closures
- Bind Context to Functions
- Collections (arrays)
- Flow Control
- Conditional statements (if…else, if…else if…else, switch)
- Iteration/Looping (for, for of, for in, do while, while)
- Higher Order Functions
- Future Values
- Callbacks
- Promises
- Async/Await

## Author

Dewald Els
