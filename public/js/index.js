const FILTER_ALL = "all"
// Source Data
const guitars = [
    "Fender Stratocaster",
    "Gibson LesPaul",  
    "Fender Telecaster"
]

// the .map() Higher Order Function
const guitarsMapped = guitars.map(function(guitar, idx) { // callback
    const guitarArray = guitar.split(" ") // Split into an array 
    
    // .map() requires a return value
    return {
        id: idx + 1,
        manufacturer: guitarArray[0],
        model: guitarArray[1],
    }
})

// DOM Elements
const guitarList = document.getElementById("guitarList")
const buyGuitarBtn = document.getElementById("buyGuitar")
const sellGuitarBtn = document.getElementById("sellGuitar")
const manufacturersSelect = document.getElementById("manufacturers")

// Function to render the guitars into the UL
function renderGuitars(guitarList, guitars) {
    guitarList.innerHTML = ""
    for (let i = 0; i < guitars.length; i++) {
        const text = guitars[i].manufacturer + " " + guitars[i].model
        // Insert HTML BEFORE the end of the UL
        guitarList.insertAdjacentHTML("beforeend", 
            "<li>" + text + "</li>"
        )
    }
}

// Trigger whenever the Select value has changed
manufacturersSelect.addEventListener("change", function() {
    const filterBy = this.value // all | Fender | Gibson
    
    if (filterBy === FILTER_ALL) {
        renderGuitars(guitarList, guitarsMapped)
        return    
    }
    
    const filteredGuitars = guitarsMapped.filter(function(guitar) {
        return guitar.manufacturer === filterBy
    })
    renderGuitars(guitarList, filteredGuitars)
})

// When ever the buyGuitar button is clicked
buyGuitarBtn.addEventListener("click", function(){
    
    
    guitars.push("Gibson Songbird")
    
    renderGuitars(guitarList, guitars)
    
})

// External event handler - This optional to a anonymous function like the previous click event
function handleSellGuitar() {
    const lastGuitar = guitars.pop() // returns the item that was removed
}

// When ever the buyGuitar button is clicked
sellGuitarBtn.addEventListener("click", handleSellGuitar)

// Initial render to display the guitars.
renderGuitars(guitarList, guitarsMapped)