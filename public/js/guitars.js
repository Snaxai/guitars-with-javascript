// URL to communicate with the API.
const URL = "https://dce-noroff-api.herokuapp.com/guitars"

// Reference to HTML DOM Elements
const guitarList = document.getElementById("guitarList")
let guitars = []

// Function to render the guitars into the DOM
function renderGuitars(guitarList, guitars) {

    for (const guitar of guitars) {
        // template literals
        const html = `
            <li>
                <img src="${guitar.image}" alt="${guitar.model}" width="200" />
                <h4>${guitar.model}</h4>
                <p>by ${guitar.manufacturer}</p>
            </li>
        `

        guitarList.insertAdjacentHTML("beforeend", html)
    }

}

fetch(URL) // fetch is part of the event loop
    .then(function(response) { // Promise resolves
        console.log("Inside Fetch")
        return response.json() // Extract the data
    })
    .then(function(_guitars) { // Received data from previous .then()'s return
        console.log(_guitars)
        guitars = _guitars
        renderGuitars(guitarList, guitars) // Before the Event Loop is checked.
    })
    .catch(function(error) { // Bad things happened
        console.error(error.message)
    })

console.log("After fetch") // Will run BEFORE the fetch() resolves.
